# -*- coding: utf-8 -*-
"""
Created on Wed Jan 18 10:02:35 2017

@author: gitecx
"""

import os
import random

import numpy  as np

def to_hot_vector(labels, clases=10):
    Class=clases+1
    label=np.asarray(labels, dtype=np.int16)
    label=np.equal.outer(label, np.arange(Class)).astype(np.float) 
    label=np.delete(label,0,1)    
    return label



def Dataset_dir_validation (dirpath):

    validation_image= []
    labels    = []

    for root, dirs, files in os.walk(dirpath,topdown=False):

        for name in files:
            
            validation_image.append(os.path.join(root, name))
            tmp_label= os.path.split(root)       
            labels.append(tmp_label[1])
    nlabels=np.asarray(labels)
    return validation_image, nlabels

def Dataset_path_validation (dirpath):
    path=os.listdir(dirpath)
    validation_image= []
    labels    = []
    
    for i in path:
        itext=dirpath+"/"+i
        File=open(itext, "r")
        if File.mode=='r':
            fl =File.readlines()  

            for x in fl:
                
                end=i.find(".")
                label=i[0:end]               
                validation_image.append(x)
                labels.append(label)

    nlabels=np.asarray(labels)

    nlabels=np.asarray(labels)
    return validation_image, nlabels

def Dataset_path_list2(dirpath,clas=10):
    path=os.listdir(dirpath)
    
    train_image  = []
    train_labels = []
       
    for i in path:
        itext=dirpath+"/"+i
        File=open(itext, "r")
        if File.mode=='r':
            fl =File.readlines()  
            labels  = []
            images  = []
            for x in fl:
                
                end=i.find(".")
                label=i[0:end]               
                images.append(x)
                labels.append(label)
      
            train_labels.append(labels)
            train_image.append(images)

    num_examples=sum([len(x) for x in train_image])
    num_test  = num_examples-int(num_examples*0.8)-1
    num=num_test/clas
     
    test_image   = [[] for i in range(len(train_image))]
    test_labels  = [[] for i in range(len(train_image))]
    
    
    for i in range(len(train_image)):
        
        train_image[i] = train_image[i][:-num]
        test_image[i]  = train_image[i][-num:]
        
        train_labels[i] = train_labels[i][:-num]
        test_labels[i]  = train_labels[i][-num:]
    
    train_image  = [j for i in train_image for j in i]   
    train_labels = [j for i in train_labels for j in i]   

    test_image  = [j for i in test_image for j in i]   
    test_labels = [j for i in test_labels for j in i]      
    
    train_labels = to_hot_vector(train_labels,clases=clas)
    test_labels  = to_hot_vector(test_labels,clases=clas)                
                
    return train_image,train_labels,test_image,test_labels    


def Dataset_path_list(dirpath,clas=10):
    path=os.listdir(dirpath)
    
    train_image  = []
    train_labels = []
    test_image   = []
    test_labels  = []
    
    nlabels = []
    indices = []
    labels2  = []
    
    for i in path:
        itext=dirpath+"/"+i
        File=open(itext, "r")
        if File.mode=='r':
            fl =File.readlines()  
            labels  = []
            for x in fl:
                
                end=i.find(".")
                label=i[0:end]               
                train_image.append(x)
                labels.append(label)
                labels2.append(label)
            nlabels.append(labels)

    num_examples=len(train_image)
    size=0
    num_test  = num_examples-int(num_examples*0.8)-1
    num=num_test/clas
    
    for i in range(clas):
        
        rand = np.arange(len(nlabels[i]))    
        rand = random.sample(rand, num) 
    
        for l in range(num):
            value=rand[l]
            if i>0:
                value=rand[l]+size
            test_image.append(train_image[value])
            indices.append(value)
            test_labels.append(labels2[value])
        size+=len(nlabels[i])
            
    labels=[i for j, i in enumerate(labels2) if j not in indices]
    train_image=[i for j, i in enumerate(train_image) if j not in indices]
    
    train_labels = to_hot_vector(labels,clases=clas)
    test_labels  = to_hot_vector(test_labels,clases=clas)                
                
    return train_image,train_labels,test_image,test_labels    


def Dataset_dir (dirpath,clas=10):

    train_image= []
    nlabels   = []
    labels    = []
    test_image= [] 
    indices   = []
    test_labels=[]
    
    
    for root, dirs, files in os.walk(dirpath,topdown=False):

        for name in files:
            
            train_image.append(os.path.join(root, name))
            tmp_label= os.path.split(root)       
            labels.append(tmp_label[1])

    num_examples=len(labels)
    size=0
    num_test  = num_examples-int(num_examples*0.8)-1
    num=num_test/clas
    for n in dirs:
        nlabels.append([i for i in labels if str(n)==i in i])

    for i in range(clas):
        
        rand = np.arange(len(nlabels[i]))    
        rand = random.sample(rand, num) 
        
        for l in range(num):
            value=rand[l]
            if i>0:
                value=rand[l]+size
            test_image.append(train_image[value])
            indices.append(value)
            test_labels.append(labels[value])
        size+=len(nlabels[i])
            
    labels=[i for j, i in enumerate(labels) if j not in indices]
    train_image=[i for j, i in enumerate(train_image) if j not in indices]
    
    train_labels = to_hot_vector(labels,clases=clas)
    test_labels  = to_hot_vector(test_labels,clases=clas)

    return train_image,train_labels,test_image,test_labels

def index(image):
    import numpy as np
    
    NDVI =(image[4]-image[2])/(image[4]+image[2])
    EVI  = 2.5 * ( image[4] - image[2] ) / ( image[4] + 6.0 * image[2] - 7.5 * image[0] + 1.0 )
    RVI  = image[4]/image[2]
    image.append(NDVI)
    image.append(EVI)
    image.append(RVI)
    return image        
 
def rasterImage (images, bands, m=True):
    from osgeo import gdal
    import numpy as np
    array_image=[]
    for name in images:
        r=[]
        driver=gdal.Open(name[:-1]) 

        for i in range(1,bands+1):
            band=driver.GetRasterBand(i)
            r.append(band.ReadAsArray()) 
            
#        r = index(r)
        array_image.append(r)
        
    myarray = np.asarray(array_image)
    
    if (m==True):
        for i in range(bands):
            m =np.mean(myarray[:,i])
            s =np.std(myarray[:,i])
            myarray[:,i]=((myarray[:,i]-m)/s)
            array_image = np.transpose(myarray, (0, 2,3,1 ))   
    else:
        array_image = np.transpose(myarray, (0, 2,3,1 ))   
    return array_image

def permutacion (lista,perm):
    lista1=[ [] for x in xrange(len(lista))]

    for i in range (len(lista)):
        lista1[i]=lista[perm[i]]
    return lista1
    
    

class DataSet(object):
    def __init__(self, images, labels, Path,fake_data=False):
        if fake_data:
            self._num_examples = images.shape[0]
        else:
            assert images.shape[0] == labels.shape[0], (
                "images.shape: %s labels.shape: %s" % (images.shape,
                                                       labels.shape))
            self._num_examples = images.shape[0]

        print images.shape[0]
#            images = images.astype(np.float32)
       
        self._images = images
        self._labels = labels         

        
        perm = np.arange(self._num_examples)
        np.random.shuffle(perm)


        self._images = self._images[perm]
        self._labels = self._labels[perm]

        self._path = permutacion(Path,perm)

        self._epochs_completed = 0
        self._index_in_epoch = 0

    @property
    def images(self):
        return self._images
    @property
    def path(self):
        return self._path 
        
    @property
    def labels(self):
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def next_batch(self, batch_size, fake_data=False):
        """Return the next `batch_size` examples from this data set."""
#        if fake_data:
#            fake_image = [1.0 for _ in xrange(1000)]
#            fake_label = 0
#            return [fake_image for _ in xrange(batch_size)], [
#                fake_label for _ in xrange(batch_size)]
        start = self._index_in_epoch

        self._index_in_epoch += batch_size
        
    
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data
            perm = np.arange(self._num_examples)
            np.random.shuffle(perm)
                                
            self._path =permutacion(self._path,perm)   
                
            self._images = self._images[perm]
            self._labels = self._labels[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch
        return self._images[start:end], self._labels[start:end], self.path[start:end]


def read_data_sets( path_dir, path_dir2, bands, fake_data=False,n_clases=10,m=True):
    class DataSets(object):
        pass
    data_sets = DataSets()
    if fake_data:
        data_sets.train = DataSet([], [], [], fake_data=True)
        data_sets.validation = DataSet([], [], [], fake_data=True)
        data_sets.test = DataSet([], [],[], fake_data=True)
        return data_sets

# train_image,train_labels,test_image,test_labels
    if (path_dir):
        
        TRAIN_IMAGES , TRAIN_LABELS, TEST_IMAGES, TEST_LABELS=Dataset_path_list2(path_dir,clas=n_clases)
        train_images = rasterImage(TRAIN_IMAGES, bands)
        test_images = rasterImage(TEST_IMAGES, bands) 
        train_labels = TRAIN_LABELS
        test_labels = TEST_LABELS
#        perm = np.arange(len(TEST_IMAGES))  
#        np.random.shuffle(perm)        
#        test_images = test_images[perm]
#        test_labels = test_labels[perm]
        data_sets.train = DataSet(train_images, train_labels,TRAIN_IMAGES)
        data_sets.test = DataSet(test_images, test_labels,TEST_IMAGES)
        
    if (path_dir2):
        VALIDATION_IMAGES , VALIDATION_LABELS = Dataset_path_validation(path_dir2)
        validation_image = rasterImage(VALIDATION_IMAGES, bands)    
        validation_labels = to_hot_vector(VALIDATION_LABELS,clases=n_clases)
#        perm = np.arange(len(VALIDATION_IMAGES))  
#        np.random.shuffle(perm)
#        validation_image = validation_image[perm] 
#        validation_labels=  validation_labels[perm]  
        data_sets.validation = DataSet(validation_image, validation_labels, VALIDATION_IMAGES)          

    return data_sets
 
    

#    
#dataset= read_data_sets(','',5,n_clases=4)  
#dataset= read_data_sets('/media/gitecx/Nuevo vol/ASIAlac/asialac/ASIALaC/Dataset_path/Training','',5,n_clases=4)  

#x,y,p=dataset.test.next_batch(20)


#        posicion=[]
#
#        while step < limit:
#            t_batch_xs, t_batch_ys, p= dataset.validation.next_batch(batch_size)
#            prediction  = tf.argmax(pred,1)
#            y_true = np.argmax(t_batch_ys,1)
#            pred_1 = prediction.eval(feed_dict={x: t_batch_xs, y: t_batch_ys, keep_prob: 1.}, session=sess)    
#            if (step ==1):
#                y_true_a+=y_true
#                y_pred_a+=pred_1              
#            else:
#                y_true_a = np.concatenate((y_true_a, y_true))
#                y_pred_a = np.concatenate((y_pred_a, pred_1))
#            
#            x=p.find('x=')
#            y=p.find('y=')
#            z=p.find('Size')
#            x0=p[x+1:y-1]
#            y0=p[y:z]
#            
#            posicion.append()
#            step+=1
#            
#            
#
#a=p[0]
#for i in range(len(p))   
#x=a.find('x=')
#y=a.find('y=')
#z=a.find('Size')
#x0=a[x+1:y-1]
#y0=a[y:z]

