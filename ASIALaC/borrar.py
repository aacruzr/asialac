# -*- coding: utf-8 -*-
"""
Created on Wed Mar  1 11:57:21 2017

@author: gitecx
"""
from osgeo import gdal
# import input_test
import io
import numpy as np
#import tensorflow as tf
import time
import dice
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def raster(r="/media/gitecx/Gits/Investigacion/Tuparro7/tuparro.vrt"):
    

   
    ds = gdal.Open(r)
    
    geo=ds.GetGeoTransform()       
    x0=geo[0]
    y0=geo[3]
    pixelwidth=geo[1]
    pixelhigh=abs(geo[5])
    width = ds.RasterXSize
    high = ds.RasterYSize    
    yi=y0-(pixelhigh*high)
    xi=x0+(pixelwidth*width)          
    return [x0,y0,xi,yi]

def maskBinary (array):

    from scipy import ndimage
    valueMin=ndimage.minimum(array)
    valueMax=ndimage.maximum(array)
    if ((valueMin ==0) | (valueMax ==255) | (valueMax ==65535) ):
        return False
    else: return True
    

class Path:
    
    def __init__(self, folder):
        from osgeo import ogr
        self.daShapefile = folder
        # Abrir el Shapefile y tipo de archivo, leer el extent
        self.shape=ogr.GetDriverByName('ESRI Shapefile')
        self.dataSource = self.shape.Open(self.daShapefile)
        self.layer = self.dataSource.GetLayer()
        self.xmin, self.xmax, self.ymin, self.ymax = self.layer.GetExtent()
        
    
    def mean_(self,array):
        if(array.mean()>0):
            return True
        else:
            return False
        
    def extractMatrix (self, pixel_size, pace, overlap, overSampling,):
        
        from  scipy import ndimage
        from scipy.ndimage import label
        overLap=0
        avr=80

        if(overlap > 0):
            if(overlap <= 90):
                overLap=(overlap*pace)/100
        # tamaño de los pixeles y valores sin dato

        pixel_size = 30
        NoData_value = 255 
        
        # crear data source de salida
        x_res = int((self.xmax - self.xmin) / pixel_size)
        y_res = int((self.ymax - self.ymin) / pixel_size)

        dataSource = gdal.GetDriverByName('MEM').Create('', x_res, y_res, gdal.GDT_Byte)
        dataSource.SetGeoTransform((self.xmin, pixel_size, 0, self.ymax, 0, -pixel_size))
        band = dataSource.GetRasterBand(1)
        band.SetNoDataValue(NoData_value)
        # rasterizar
        gdal.RasterizeLayer(dataSource, [1], self.layer, burn_values=[1])        
        # leer como array
        geo = dataSource.GetGeoTransform()  
        array = band.ReadAsArray()
        arraylist=[]  
        step=int(pace)    
        labeled_array, num_features = label(array)
        count=ndimage.find_objects(labeled_array)
       
        for tuples in count:   
            for l in xrange(tuples[0].start,tuples[0].stop,int(step-overLap)):
                for s in xrange(tuples[1].start,tuples[1].stop,step-overLap):
                    if(overSampling==False):                    
                        A=array[l:l+step, s:s+step]  
                        xmin=(s*pixel_size)+geo[0]
                        ymax=geo[3]-(pixel_size*l)
                        f=self.mean_(A)
                        if (f==True):
                            arraylist.append([xmin,ymax])
                        
        return arraylist,array
 








def signature_classification(m,s,signatures):
    
    spectral=signatures.files
    class_=np.zeros([4,7])
   
    for i in range(4):
        b = signatures[spectral[i]]  
        
        for l in range(7):
            vmax = b[l,0] + b[l,1]
            vmin = b[l,0] - b[l,1]
            jj=m[l]
            
            if ((m[l] > vmin) & (m[l] < vmax)):
                class_[i][l]=1
                
    labels = np.sum(class_,axis=1)
    if (labels[0]==6):
        return 1
    if (labels[1]==6):
        return 2
    if (labels[2]==6):
        return 3
    if (labels[3]==6):
        return 4
    
    else:
        return 0
    


def flight_signature(r,shape):
    signatures = np.load("/media/gitecx/Nuevo vol/ASIAlac/asialac/ASIALaC/Spectral Signatures Classes.npz")

    print "Firmas espectrales cargadas correctamente"

    a = Path(shape)
    position, array = a.extractMatrix(30,1,0, overSampling=False) 
    predictions = []
    solo_labels = []
    rlimit = raster(r=r)  
    
    x_res = int((a.xmax - a.xmin) / 30)
    y_res = int((a.ymax - a.ymin) / 30)
    
    limit = (int((a.xmin - rlimit[0]) / 30))
    limit2 = abs(int((a.ymax - rlimit[1]) / 30))

    ds = gdal.Open(r)
    bands=[]
    for i in range(ds.RasterCount):   
        band = ds.GetRasterBand(i+1)
        ra=band.ReadAsArray(limit,limit2,x_res,y_res)
        ra=ra*array
        bands.append(ra) 
    myarray = np.asarray(bands)  
    print len(position)

    print myarray.max()
    for i in range(len(position)):
        
        px = position[i][0]
        py = position[i][1]  
        
        xOffset = int((px - a.xmin) / 30)
        yOffset = int((py - a.ymax) / (-30))
          
        image = myarray[:,yOffset:yOffset+1, xOffset:xOffset+1]
        
        m = []
        s = []
        for k in range(7):
            
            m.append(np.mean(image[k:,:]))
            s.append(np.std(image[k:,:]))
        
        labels = signature_classification(m,s,signatures )  
        
        predictions.append([[px],[py],labels])
        solo_labels.append(labels)
        
    resolution=x_res*y_res
 
    
    map_out = np.zeros(resolution).reshape(y_res,x_res)
    print max(solo_labels)
    for i in range (len(predictions)):
        xOffset = abs(int((float(predictions[i][0][0]) - a.xmin) / 30))
        yOffset = abs(int((float(predictions[i][1][0]) - a.ymax) / 30))

        k = predictions[i][2]
        y2=yOffset+20
        x2=xOffset+20
    
        if (x2<x_res):
            map_out[ yOffset:y2 , xOffset:x2 ] = k

    new_map=map_out*array
    c_dice,mask=dice.coefficient(new_map,shape)
    mask.append(new_map)
 

    print "Coeficientes de Dice para la clase 1 %.3f clase 2 %.3f clase %.3f clase %.3f" % (c_dice[0], c_dice[1], c_dice[2], c_dice[3])   
  
    for i in range(5):
        if(i==4):
            print ("Predición de mapa al vuelo ")
        if (i==1):
            print "Mascaras Binarias por clase "
            
        plt.imshow(mask[i])
        plt.figure(i+1)
    plt.show()        
    
    
    
def flight(r,shape):
    posiciones=[]
    a = Path(shape)
    position, array = a.extractMatrix(30,20,50, overSampling=False) 
    predictions = []
 
    rlimit = raster(r=r)  
    prediction  = tf.argmax(pred,1)
    
    x_res = int((a.xmax - a.xmin) / 30)
    y_res = int((a.ymax - a.ymin) / 30)
    
    limit = (int((a.xmin - rlimit[0]) / 30))
    limit2 = abs(int((a.ymax - rlimit[1]) / 30))

    ds = gdal.Open(r)
    bands=[]
    for i in range(ds.RasterCount):   
        band = ds.GetRasterBand(i+1)
        ra=band.ReadAsArray(limit,limit2,x_res,y_res)
        ra=ra*array
        bands.append(ra) 
    myarray = np.asarray(bands)  
    
    for i in range(1,8):
        m =np.mean(myarray[:,i])
        s =np.std(myarray[:,i])
        myarray[:,i]=((myarray[:,i]-m)/s)

    with tf.Session() as sess :
        sess.run(init)
        saver.restore(sess,model_path)   
        print  ("Modelo restaurado con exito")
        for i in range(len(position)):
            px = position[i][0]
            py = position[i][1]  
            xOffset = int((px - a.xmin) / 30)
            yOffset = int((py - a.ymax) / (-30))
          
            image = myarray[:,yOffset:yOffset+20, xOffset:xOffset+20]
            image = np.expand_dims(image, axis=0)
            image = np.transpose(image, (0, 2,3,1 ))  
    
            if (image.shape == (1,20,20,7)):       
                labels =  prediction.eval(feed_dict={x: image, keep_prob: 1.}, session=sess)    
                predictions.append([[px],[py],labels])

        sess.close    
    resolution=x_res*y_res

    map_out = np.zeros(resolution).reshape(y_res,x_res)

    for i in range (len(predictions)):
        xOffset = abs(int((float(predictions[i][0][0]) - a.xmin) / 30))
        yOffset = abs(int((float(predictions[i][1][0]) - a.ymax) / 30))

        k = predictions[i][2] + 1      
        y2=yOffset+20
        x2=xOffset+20
    
        if (x2<x_res):
            map_out[ yOffset:y2 , xOffset:x2 ] = k

    new_map=map_out*array
    c_dice,mask=dice.coefficient(new_map,shape)
    mask.append(new_map)
 

    print "Coeficientes de Dice para la clase 1 %.3f clase 2 %.3f clase %.3f clase %.3f" % (c_dice[0], c_dice[1], c_dice[2], c_dice[3])   
  
    for i in range(5):
        if(i==4):
            print ("Predición de mapa al vuelo ")
        if (i==1):
            print "Mascaras Binarias por clase "
            
        plt.imshow(mask[i])
        plt.figure(i+1)
    plt.show()  

flight_signature("/media/gitecx/Gits/Investigacion/Tuparro7/tuparro.vrt","/media/gitecx/Nuevo vol/Untitled Folder/000Nueva carpeta2/iMAGNEES DE SALIDA/s_out/Final Shape's/tupa.shp")

