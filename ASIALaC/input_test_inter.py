# -*- coding: utf-8 -*-
"""
Created on Sun Sep 25 10:51:09 2016

@author: arnol
"""

import os
import random

import numpy  as np

def to_hot_vector(labels, clases=10):
    Class=clases+1
    label=np.asarray(labels, dtype=np.int16)
    label=np.equal.outer(label, np.arange(Class)).astype(np.float) 
    label=np.delete(label,0,1)    
    return label



def Dataset_dir_validation (dirpath):

    validation_image= []
    labels    = []

    for root, dirs, files in os.walk(dirpath,topdown=False):

        for name in files:
            
            validation_image.append(os.path.join(root, name))
            tmp_label= os.path.split(root)       
            labels.append(tmp_label[1])
    nlabels=np.asarray(labels)
    return validation_image, nlabels

def Dataset_dir (dirpath,clas=10):

    train_image= []
    nlabels   = []
    labels    = []
    test_image= [] 
    indices   = []
    test_labels=[]
    
    for root, dirs, files in os.walk(dirpath,topdown=False):

        for name in files:
            
            train_image.append(os.path.join(root, name))
            tmp_label= os.path.split(root)       
            labels.append(tmp_label[1])

    num_examples=len(labels)
    size=0
    num_test  = num_examples-int(num_examples*0.9)-1
    num=num_test/clas
    for n in dirs:
        nlabels.append([i for i in labels if str(n)==i in i])

    for i in range(clas):
        
        rand = np.arange(len(nlabels[i]))    
        rand = random.sample(rand, num) 
        
        for l in range(num):
            value=rand[l]
            if i>0:
                value=rand[l]+size
            test_image.append(train_image[value])
            indices.append(value)
            test_labels.append(labels[value])
        size+=len(nlabels[i])
            
    labels=[i for j, i in enumerate(labels) if j not in indices]
    train_image=[i for j, i in enumerate(train_image) if j not in indices]
    
    train_labels = to_hot_vector(labels,clases=clas)
    test_labels  = to_hot_vector(test_labels,clases=clas)

    return train_image,train_labels,test_image,test_labels
    
def generador_m_s (images, bands, m=True):
    print (len(images))
    from osgeo import gdal
    import numpy as np
    array_image=[]
    for name in images:
        r=[]
        driver=gdal.Open(name)   
        for i in range(1,bands+1):
            band=driver.GetRasterBand(i)
            r.append(band.ReadAsArray()) 
                
        array_image.append(r)
    myarray = np.asarray(array_image)
    
    if (m==True):
        for i in range(bands):
            m =np.mean(myarray[:,i])
            s =np.std(myarray[:,i])
            
            print "La media y la desviación estandar de los datos de entrenamiento en la capa es m = {} y std = {} ".format(m,s)    
    

def rasterImage (images, bands, m=True):
    from osgeo import gdal
    import numpy as np
    array_image=[]
    for name in images:
        r=[]
        driver=gdal.Open(name)   
        for i in range(1,bands+1):
            band=driver.GetRasterBand(i)
            r.append(band.ReadAsArray()) 
                
        array_image.append(r)
    myarray = np.asarray(array_image)
    
#    norm = [[66.0256576538,9.78121852875],
#          [30.048833847,7.67984247208],
#          [29.3293247223,12.6006278992],
#          [54.2133483887,21.600692749],
#          [72.5908813477,35.8457832336],
#          [135.637451172,6.49729347229],
#          [29.8476581573,19.1047420502]]


#   Normalizacion sin artefactos
          
#    norm =[[65.5665740967,7.59398984909 ],
#          [29.6208076477,6.93409013748],
#          [29.2007141113,12.0752677917],
#          [51.8405570984,21.7817344666],
#          [71.6743240356,35.8052330017],
#          [136.855209351,5.64930200577],
#          [30.4583797455,19.0720615387]]
          
          
#     Normalizacion sin artefactos y TOA          
    norm = [[0.0956493616104,0.0107377385721],
            [0.0848653763533,0.0197297353297],
            [0.0725898742676,0.0294537376612],
            [0.17284463346,0.0683330073953],
            [0.152564808726,0.0728877186775],
            [0.086240015924,0.0554821528494]]

    if (m==True):
        for i in range(bands):            
            myarray[:,i]=((myarray[:,i]-norm[i][0])/norm[i][1])
        array_image = np.transpose(myarray, (0, 2,3,1 ))   
    else:
        print "No normalizado"
        array_image = np.transpose(myarray, (0, 2,3,1 ))   
        
    return array_image

def permutacion (lista,perm):
    lista1=[ [] for x in xrange(len(lista))]

    for i in range (len(lista)):
        lista1[i]=lista[perm[i]]
    return lista1
    
    

class DataSet(object):
    def __init__(self, images, labels, Path,fake_data=False):
        if fake_data:
            self._num_examples = images.shape[0]
        else:
            assert images.shape[0] == labels.shape[0], (
                "images.shape: %s labels.shape: %s" % (images.shape,
                                                       labels.shape))
            self._num_examples = images.shape[0]

        print images.shape[0]
#            images = images.astype(np.float32)
       
        self._images = images
        self._labels = labels         

        
        perm = np.arange(self._num_examples)
        np.random.shuffle(perm)


        self._images = self._images[perm]
        self._labels = self._labels[perm]

        self._path = permutacion(Path,perm)

        self._epochs_completed = 0
        self._index_in_epoch = 0

    @property
    def images(self):
        return self._images
    @property
    def path(self):
        return self._path 
        
    @property
    def labels(self):
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def next_batch(self, batch_size, fake_data=False):
        """Return the next `batch_size` examples from this data set."""
#        if fake_data:
#            fake_image = [1.0 for _ in xrange(1000)]
#            fake_label = 0
#            return [fake_image for _ in xrange(batch_size)], [
#                fake_label for _ in xrange(batch_size)]
        start = self._index_in_epoch

        self._index_in_epoch += batch_size
        
    
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data
            perm = np.arange(self._num_examples)
            np.random.shuffle(perm)
                                
            self._path =permutacion(self._path,perm)   
                
            self._images = self._images[perm]
            self._labels = self._labels[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch
        return self._images[start:end], self._labels[start:end], self.path[start:end]


def read_data_sets( path_dir, path_dir2, bands, fake_data=False,n_clases=10,mi=True):
    class DataSets(object):
        pass
    data_sets = DataSets()
    if fake_data:
        data_sets.train = DataSet([], [], [], fake_data=True)
        data_sets.validation = DataSet([], [], [], fake_data=True)
        data_sets.test = DataSet([], [],[], fake_data=True)
        return data_sets
        
        
##############################
        
# train_image,train_labels,test_image,test_labels
    if (path_dir):
#        VALIDATION_IMAGES , VALIDATION_LABELS = Dataset_dir_validation(path_dir)
#        
#        generador_m_s(VALIDATION_IMAGES,bands)
        
        TRAIN_IMAGES , TRAIN_LABELS, TEST_IMAGES, TEST_LABELS=Dataset_dir(path_dir,clas=n_clases)               

        train_images = rasterImage(TRAIN_IMAGES, bands,m=mi)
        test_images = rasterImage(TEST_IMAGES, bands,m=mi) 
        train_labels = TRAIN_LABELS
        test_labels = TEST_LABELS
#        perm = np.arange(len(TEST_IMAGES))  
#        np.random.shuffle(perm)        
#        test_images = test_images[perm]
#        test_labels = test_labels[perm]
        data_sets.train = DataSet(train_images, train_labels,TRAIN_IMAGES)
        data_sets.test = DataSet(test_images, test_labels,TEST_IMAGES)
        
    if (path_dir2):
        
        VALIDATION_IMAGES , VALIDATION_LABELS = Dataset_dir_validation(path_dir2)
        validation_image = rasterImage(VALIDATION_IMAGES, bands,m=mi)    
        validation_labels = to_hot_vector(VALIDATION_LABELS,clases=n_clases)
#        perm = np.arange(len(VALIDATION_IMAGES))  
#        np.random.shuffle(perm)
#        validation_image = validation_image[perm] 
#        validation_labels=  validation_labels[perm]  
        data_sets.validation = DataSet(validation_image, validation_labels, VALIDATION_IMAGES)          

    return data_sets
    
#    
#a= "/media/gitecx/Nuevo vol/ASIAlac/Patch/Clases_TOA/Entrenamiento"
#dataset= read_data_sets(a,'',6,n_clases=4)
##dataset= read_data_sets('/media/gitecx/Nuevo vol/ASIAlac/Dataset/Dataset_sin_Artefactos','',7,n_clases=4)