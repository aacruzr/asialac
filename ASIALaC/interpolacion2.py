# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal
"""
from osgeo import ogr
from osgeo import gdal
import matplotlib.pyplot as plt
import numpy as np



a = r'/media/gitecx/Gits/AmazonasCober/shape_Cober_Magna/Tri-capa'
shapeFiles=[]
import os
for root, dirs, files in os.walk(a):
    for name in files:
        if(name.find(".shp") > 0):
            shapeFiles.append(os.path.join(root, name))

def imageShape(shape):
    
# Define pixel_size and NoData value of new raster
    pixel_size = 30
    NoData_value = 255

# Open the data source and read in the extent
    
    source_ds = ogr.Open(a)
    source_layer = source_ds.GetLayer()
    source_srs = source_layer.GetSpatialRef()
    x_min, x_max, y_min, y_max = source_layer.GetExtent()
#    x_min = 851378.762225
#    x_max = 996371.541852
#    y_min = 788352.912668
#    y_max = 905914.211906
    print x_min
    print x_max
    print y_min
    print y_max
#    
# Create the destination data source
    x_res = int((x_max - x_min) / pixel_size)
    y_res = int((y_max - y_min) / pixel_size)
    target_ds = gdal.GetDriverByName('MEM').Create('', x_res, y_res, gdal.GDT_Byte)
    target_ds.SetGeoTransform((x_min, pixel_size, 0, y_max, 0, -pixel_size))
    band = target_ds.GetRasterBand(1)
    band.SetNoDataValue(NoData_value)
    posicion=[[x_min],[y_max]]
# Rasterize
    gdal.RasterizeLayer(target_ds, [1], source_layer, burn_values=[1])

# Read as array
    array = band.ReadAsArray()
    target_ds = None
    return array,posicion
    

a='/media/gitecx/Gits/AmazonasCober/shape_Cober_Magna/Herbazal_abierto_arenoso_2012/Herbazal_abierto_arenoso.shp'
n,_=imageShape(a)    
matrix=[]
posicion=[]

for i in shapeFiles:
    print i
    m,p=imageShape(i)
    matrix.append(m)
    posicion.append(p)
c=[]    
for l in range(len(matrix)):
    o=matrix[l]
    o[o > 0] = l+1
    c.append(o)

plt.matshow(c[1])


plt.show()