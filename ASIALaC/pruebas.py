# -*- coding: utf-8 -*-
"""
Created on Thu Apr 07 09:04:36 2016

@author: arnol
"""
def createTiff(folders, parche, x0, y0, pixel_size, step, difBan):
    import osr
    import os 
    from osgeo import gdal
    array=parche
    name= "x="+str(x0)+",y="+str(y0)+"step="+str(step)+difBan+".tif"
    #format = "GTiff"
    direccion =os.path.join(folders,name)
    driver = gdal.GetDriverByName( "GTiff" )
    #metadata = driver.GetMetadata()
    dst_ds = driver.Create( direccion, step, step, len(parche) , gdal.GDT_Float32 )
    spatialReference =osr.SpatialReference() 
    spatialReference.ImportFromEPSG(3118)
    dst_ds.SetGeoTransform( [ x0, pixel_size, 0, y0, 0, -pixel_size] )
    srs = osr.SpatialReference()
    dst_ds.SetProjection( srs.ExportToWkt() )
    
    for i in range(len(array)):
        j=i+1
        dst_ds.GetRasterBand(j).WriteArray( array[i] )
    # Once we're done, close properly the dataset
    dst_ds = None
    
class Directory:
    
    def __init__(self):
        self.x0=0
        self.y0=0
        self.xi=0
        self.yi=0
        self.width=0
        self.high=0
        self.pixelwidth=0
        self.pixelhigh=0        
        self.band=0
        self.address=""
#        self.xMax=0
#        self.yMax=0
#        self.xMin=0
#        self.yMin=0
        
    
        
    
    def Bands( self,sourceimage):
        
        import numpy, os
        t=True
        cha = ['1','2','3','4','5','6','7','8','9','10','11','12','8A']        
        dirs = os.listdir( sourceimage)  
        
        bands=[]        
        for filename in dirs:
            if ( (filename.find("aux.xml") < 0) & ( (filename[-4:]==".tif") | (filename[-4:]==".TIF")  )):
                bands.append(sourceimage+"/"+filename)                
        bands.sort()    
      # self.Features(bands[0])
        return bands
        
        
    def Features(self, dlist):
        
        self.address=dlist
        from osgeo import gdal
        dataset=gdal.Open(dlist)
        rband=dataset.GetRasterBand(1)
        geo=dataset.GetGeoTransform()       
        self.x0=geo[0]
        self.y0=geo[3]
        self.pixelwidth=geo[1]
        self.pixelhigh=abs(geo[5])
        self.width = dataset.RasterXSize
        self.high = dataset.RasterYSize    
        self.yi=self.y0-(self.pixelhigh*self.high)
        self.xi=self.x0+(self.pixelwidth*self.width)
         

    def Search(self):
        sourceimage=[]
        c=[]
        import os
        from Tkinter import Tk
        import Tkconstants, tkFileDialog
        from tkFileDialog import askopenfilename     
        t=True
        root = Tk() 
        root.withdraw()
        a = tkFileDialog.askdirectory(parent=root,initialdir=".",title='Please select a directory',mustexist=True)        
        for x in os.walk(a):
            if(x!=a):
                sourceimage.append(x[0])     
        for folders in range (1,len(sourceimage)):
            c.append(self.Bands(sourceimage[folders]) )
        return c

        
    def searchFiles (self):
        shapeFiles=[]
        import os
        from Tkinter import Tk
        import Tkconstants, tkFileDialog
        from tkFileDialog import askopenfilename     

        root = Tk() 
        root.withdraw()
        a = tkFileDialog.askdirectory(parent=root,initialdir=".",title='Please select a directory',mustexist=True)        
        for root, dirs, files in os.walk(a):
            for name in files:
                if(name.find(".shp") > 0):
                    shapeFiles.append(os.path.join(root, name))
        shapeFiles=[u'I:/Investigacion/Shape/Shape Altillanura/aa\\Afloramientos rocosos\\Afloramientos rocosos.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Bosque de galer\xeda y ripario\\Bosque de galer\xeda y ripario.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Bosque denso alto de tierra firme\\Bosque denso alto de tierra firme.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Bosque denso alto inundable\\Bosque denso alto inundable.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Herbazal Abierto Arenoso\\Herbazal Abierto Arenoso.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Herbazal denso de tierra firme no arbolado\\Herbazal denso de tierra firme no arbolado.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Herbazal denso inundable arbolado\\Herbazal denso inundable arbolado.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Herbazal denso inundable no arbolado\\Herbazal denso inundable no arbolado.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Lagunas, lagos y ci\xe9nagas naturales\\Lagunas, lagos y ci\xe9nagas naturales.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Mosaico de pastos con espacios naturales\\Mosaico de pastos con espacios naturales.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\R\xedos\\R\xedos.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Vegetaci\xf3n secundaria  o en transici\xf3n\\Vegetaci\xf3n secundaria  o en transici\xf3n.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Zonas arenosas naturales\\Zonas arenosas naturales.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Zonas quemadas\\Zonas quemadas.shp']           
        return shapeFiles



class Mcobertura:
    
    def __init__(self):
        from osgeo import ogr
        import os
#        h=Directory()
        
        os.chdir(r"I:\Investigacion\Shape\Shape Altillanura\Tuparro")
        self.daShapefile = r"I:\Investigacion\Shape\Shape Altillanura\Tuparro\Tuparro.shp"
        # Abrir el Shapefile y tipo de archivo
        self.shape=ogr.GetDriverByName('ESRI Shapefile')
        self.dataSource = self.shape.Open(self.daShapefile, 0)
        # Guarda el numero de layers ("Caracteristicas") del poligono
        self.layer = self.dataSource.GetLayer()
        self.numPolygon = self.layer.GetFeatureCount()
        # Guarda los nombres de las columnas
        self.layerDefinition = self.layer.GetLayerDefn()
        
        self.outShapefile ="" 
        self.outDriver = ogr.GetDriverByName("ESRI Shapefile")

    def contar(self,elemento, lista):
        veces = 0
        for i in lista:
            if elemento == i:
                veces += 1
        return veces

    def nameColum(self):  
        columnas=[]        
        for i in range(self.layerDefinition.GetFieldCount()):
            columnas.append(self.layerDefinition.GetFieldDefn(i).GetName())
        return columnas
      
    def numeroClases(self):
        # Direccion de los archivos          
        s = "LEYENDA"
        ss=[]
        rlist=set()
        for feature in self.layer:
            ss.append(feature.GetField(s))
           
        rlist = [x for x in ss if x not in rlist and not rlist.add(x)]    
        nClases=[]
        for i in rlist:
            nClases.append([i,self.contar(i,ss)])
       
        return nClases                 
        
    def polyShape(self, a, b):
        from osgeo import ogr
        import os
        self.outShapefile =os.path.join( os.path.split( self.daShapefile )[0], b )
        self.layer.SetAttributeFilter("minor = 'HYDR'")
        if os.path.exists(self.outShapefile):
            self.outDriver.DeleteDataSource(self.outShapefile)

        outDataSource = self.outDriver.CreateDataSource(self.outShapefile)
        outShape = os.path.splitext( os.path.split( self.outShapefile )[1] )[0]
        outLayer = outDataSource.CreateLayer( outShape, geom_type=ogr.wkbMultiPolygon )
        
        outLayerDefn = outLayer.GetLayerDefn()
        for inFeature in self.layer:     
            # Create output Feature
            if inFeature.GetField("LEYENDA")==b:          
                outFeature = ogr.Feature(outLayerDefn)
                # Set geometry as centroid
                geom = inFeature.GetGeometryRef()
                outFeature.SetGeometry(geom.Clone())
                # Add new feature to output Layer
                outLayer.CreateFeature(outFeature)
                # Close DataSources
        #self.shape.Destroy()
        outDataSource.Destroy()
        
        
        
class Patch (Directory):
    
    def __init__(self, folder):
        from osgeo import ogr
        self.daShapefile = folder
        # Abrir el Shapefile y tipo de archivo, leer el extent
        self.shape=ogr.GetDriverByName('ESRI Shapefile')
        self.dataSource = self.shape.Open(self.daShapefile)
        self.layer = self.dataSource.GetLayer()
        self.xmin, self.xmax, self.ymin, self.ymax = self.layer.GetExtent()
        
#############################################################################################
       
    def extractMatrix (self, pixel_size, pace, overlap):
        from osgeo import gdal
        from  scipy import ndimage
        from scipy.ndimage import label
        
        if(overlap > 0):
            if(overlap <= 90):
                overlap=(overlap*pace)/100
        # tamaño de los pixeles y valores sin dato
        pixel_size = 30
        NoData_value = 255 
        # crear data source de salida
        x_res = int((self.xmax - self.xmin) / pixel_size)
        y_res = int((self.ymax - self.ymin) / pixel_size)
        dataSource = gdal.GetDriverByName('MEM').Create('', x_res, y_res, gdal.GDT_Byte)
        dataSource.SetGeoTransform((self.xmin, pixel_size, 0, self.ymax, 0, -pixel_size))
        band = dataSource.GetRasterBand(1)
        band.SetNoDataValue(NoData_value)
        # rasterizar
        gdal.RasterizeLayer(dataSource, [1], self.layer, burn_values=[1])        
        # leer como array
        geo = dataSource.GetGeoTransform()  
        array = band.ReadAsArray()
        arraylist=[]  
        step=int(pace)    
        labeled_array, num_features = label(array)
        count=ndimage.find_objects(labeled_array)
        
        for tuples in count:
            
            for l in xrange(tuples[0].start,tuples[0].stop,int(step-overlap)):
            
                for s in xrange(tuples[1].start,tuples[1].stop,step-overlap):
                    A=array[l:l+step, s:s+step]
               
                    if (self.sumM(A) >=80 ):                              
                        xmin=(s*pixel_size)+geo[0]
                        ymax=geo[3]-(pixel_size*l)
                        arraylist.append([xmin,ymax])
        return arraylist   

        
    def sumM (self, matrix):
        percent=0
        m=len(matrix) * len(matrix[0])
        for j in range (len(matrix)):
            for f in range (len(matrix[0])):
                if (matrix[j][f]>0):
                    percent+=1
        out=(100 * percent) / m 
        return out
        
        
        
################### GetAttribute #####################
class GetAttribute(Directory, Patch):
    def __init__(self):
        self.atributos=[]
        self.shapeFiles=[]
    def listRaster(self):
        print "Select the folder with the raster "
        h=self.Search()
        self.atributos=[[]for _ in range(len(h)) ]

        for x in range (0,len(h)):
            for l in range (0, len(h[x])):
                j=Directory() 
                j.Features(h[x][l])
                self.atributos[x].append(j)
        print "Select the shape files to use "
        self.shapeFiles=self.searchFiles()
        return  self.atributos
        
    
    def createFolder (self, shapeFiles, class_number ):
        import os      
        f=os.path.split(shapeFiles)
        class_name=f[1][:-4]
        number=class_number+1
        directoryFolder="D:/"+"Patch/"+"Class "+"0"+str(number)+"_"+class_name
        if not os.path.exists(directoryFolder):
            os.makedirs(directoryFolder)
        return directoryFolder
        
    def runTime (self, step, overlap, pixel_size):
        folders=""
        for folder in self.shapeFiles:
            count=self.shapeFiles.index(folder)
            Patch.__init__(self,folder)           
            folders=self.createFolder(folder,count)    
            position=self.extractMatrix(pixel_size,step,overlap)    
            
            if(count >= 1):
                print folder
            for i in range (len(position)):
                sampleClass, difBan = self.validatePosition(position[i][0],position[i][1],pixel_size,step) 
                if(len(sampleClass) > 0):
                    createTiff(folders,sampleClass,position[i][0],position[i][1],pixel_size,step, difBan)
      
    def validatePosition(self, x, y, pixel_size, step):
        arrayP=[]  
        difBan=""
        from osgeo import gdal
        for i in range(len(self.atributos)):
            for l in range(len(self.atributos[i])):
                xi=x+(step*self.atributos[i][l].pixelwidth)
                yi=y-(step*self.atributos[i][l].pixelwidth)
                dataset=gdal.Open(self.atributos[i][l].address)
                rband=dataset.GetRasterBand(1)                 
                if( (x > self.atributos[i][l].x0) & (xi < self.atributos[i][l].xi) & (y < self.atributos[i][l].y0) & (yi > self.atributos[i][l].yi)):
 
                    if((self.atributos[i][l].pixelwidth < 31) ):
                   
                        xOffset = int((x - self.atributos[i][l].x0) /self.atributos[i][l].pixelwidth)
                        yOffset = int((y - self.atributos[i][l].y0) / (-self.atributos[i][l].pixelwidth))
                        array=rband.ReadAsArray(xOffset,yOffset,step,step)                           

                        t= self.maskBinary(array)
                        if (t==True):                
                            arrayP.append(array)
                            difBan=str(i)+","+str(l)  
                    dataset= None  
        return arrayP, difBan

    def maskBinary (self, array):
        from scipy import ndimage
        valueMin=ndimage.minimum(array)
        valueMax=ndimage.maximum(array)
        if ((valueMin ==0) | (valueMax ==255) | (valueMax ==65535) ):
            return False
        else: return True
        
    def increaseArrayR(self, array, scalar,step): 
        import numpy as np
        arrayI=np.repeat(array,scalar)
        arrayOut=arrayI.reshape(step,step)
        return arrayOut


a=GetAttribute()
c=a.listRaster()
#    def runTime (self, step, overlap, pixel_size):
a.runTime(20,50,30.0)


#####################################


from osgeo import ogr
from osgeo import osr
areasT=[]
h=Directory()
shapeFiles=h.searchFiles()
print shapeFiles
for i in range(len(shapeFiles)):
    shape=ogr.GetDriverByName('ESRI Shapefile')
    dataSource =shape.Open(shapeFiles[i])
    layer =dataSource.GetLayer()

    source = osr.SpatialReference()
    source.ImportFromEPSG(2927)
    
    target = osr.SpatialReference()
    target.ImportFromEPSG(3118)

        # Abrir el Shapefile y tipo de archivo, leer el extent

    total=0

    for feature in layer:
        geom = feature.GetGeometryRef()
        #    spatialRef = geom.GetSpatialReference()
        #    transform = osr.CoordinateTransformation(source,target)
        #    geom.Transform( transform)
        spatialRef = geom.GetSpatialReference()
        area = geom.GetArea() 

        total+=area
    areasT.append(total)
        
   
