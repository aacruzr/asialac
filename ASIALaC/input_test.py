# -*- coding: utf-8 -*-
"""
Created on Mon May 30 11:38:17 2016

@author: arnol
"""
import os
import random

import numpy  as np

def to_hot_vector(labels, clases=10):
    Class=clases+1
    label=np.asarray(labels, dtype=np.int16)
    label=np.equal.outer(label, np.arange(Class)).astype(np.float) 
    label=np.delete(label,0,1)    
    return label



def Dataset_dir_validation (dirpath):

    validation_image= []
    labels    = []

    for root, dirs, files in os.walk(dirpath,topdown=False):

        for name in files:
            
            validation_image.append(os.path.join(root, name))
            tmp_label= os.path.split(root)       
            labels.append(tmp_label[1])
    nlabels=np.asarray(labels)
    return validation_image, nlabels

def Dataset_dir (dirpath,clas=10):

    train_image= []
    nlabels   = []
    labels    = []
    test_image= [] 
    indices   = []
    test_labels=[]
    
    for root, dirs, files in os.walk(dirpath,topdown=False):

        for name in files:
            
            train_image.append(os.path.join(root, name))
            tmp_label= os.path.split(root)       
            labels.append(tmp_label[1])

    num_examples=len(labels)
    size=0
    num_test  = num_examples-int(num_examples*0.9)-1
    num=num_test/clas
    for n in dirs:
        nlabels.append([i for i in labels if str(n)==i in i])

    for i in range(clas):
        
        rand = np.arange(len(nlabels[i]))    
        rand = random.sample(rand, num) 
        
        for l in range(num):
            value=rand[l]
            if i>0:
                value=rand[l]+size
            test_image.append(train_image[value])
            indices.append(value)
            test_labels.append(labels[value])
        size+=len(nlabels[i])
            
    labels=[i for j, i in enumerate(labels) if j not in indices]
    train_image=[i for j, i in enumerate(train_image) if j not in indices]
    
    train_labels = to_hot_vector(labels,clases=clas)
    test_labels  = to_hot_vector(test_labels,clases=clas)

    return train_image,train_labels,test_image,test_labels

def rasterImage (images, bands, m=True):
    from osgeo import gdal
    import numpy as np
    array_image=[]
    for name in images:
        r=[]
        driver=gdal.Open(name)   
        for i in range(1,bands+1):
            band=driver.GetRasterBand(i)
            r.append(band.ReadAsArray()) 
        array_image.append(r)
    myarray = np.asarray(array_image)
    if (m==True):
        for i in range(bands):
            m =np.mean(myarray[:,i])
            s =np.std(myarray[:,i])
            myarray[:,i]=((myarray[:,i]-m)/s)
            array_image = np.transpose(myarray, (0, 2,3,1 ))   
    else:
        array_image = np.transpose(myarray, (0, 2,3,1 ))   
    return array_image



class DataSet(object):
    def __init__(self, images, labels, fake_data=False):
        if fake_data:
            self._num_examples = images.shape[0]
        else:
            assert images.shape[0] == labels.shape[0], (
                "images.shape: %s labels.shape: %s" % (images.shape,
                                                       labels.shape))
            self._num_examples = images.shape[0]

        print images.shape[0]
#            images = images.astype(np.float32)
       
        self._images = images
        self._labels = labels         
         
        perm = np.arange(self._num_examples)
        np.random.shuffle(perm)
        
        self._images = self._images[perm]
        self._labels = self._labels[perm]

        self._epochs_completed = 0
        self._index_in_epoch = 0

    @property
    def images(self):
        return self._images

    @property
    def labels(self):
        return self._labels

    @property
    def num_examples(self):
        return self._num_examples

    @property
    def epochs_completed(self):
        return self._epochs_completed

    def next_batch(self, batch_size, fake_data=False):
        """Return the next `batch_size` examples from this data set."""
#        if fake_data:
#            fake_image = [1.0 for _ in xrange(1000)]
#            fake_label = 0
#            return [fake_image for _ in xrange(batch_size)], [
#                fake_label for _ in xrange(batch_size)]
        start = self._index_in_epoch

        self._index_in_epoch += batch_size
        
    
        if self._index_in_epoch > self._num_examples:
            # Finished epoch
            self._epochs_completed += 1
            # Shuffle the data
            perm = np.arange(self._num_examples)
            np.random.shuffle(perm)
            self._images = self._images[perm]
            self._labels = self._labels[perm]
            # Start next epoch
            start = 0
            self._index_in_epoch = batch_size
            assert batch_size <= self._num_examples
        end = self._index_in_epoch
        return self._images[start:end], self._labels[start:end]


def read_data_sets( path_dir, path_dir2, bands, fake_data=False,n_clases=10,m=True):
    class DataSets(object):
        pass
    data_sets = DataSets()
    if fake_data:
        data_sets.train = DataSet([], [], fake_data=True)
        data_sets.validation = DataSet([], [], fake_data=True)
        data_sets.test = DataSet([], [], fake_data=True)
        return data_sets
        
        
##############################
        
# train_image,train_labels,test_image,test_labels
    if (path_dir):
        
        TRAIN_IMAGES , TRAIN_LABELS, TEST_IMAGES, TEST_LABELS=Dataset_dir(path_dir,clas=n_clases)
        train_images = rasterImage(TRAIN_IMAGES, bands)
        test_images = rasterImage(TEST_IMAGES, bands) 
        train_labels = TRAIN_LABELS
        test_labels = TEST_LABELS
        perm = np.arange(len(TEST_IMAGES))  
        np.random.shuffle(perm)        
        test_images = test_images[perm]
        test_labels = test_labels[perm]
        data_sets.train = DataSet(train_images, train_labels)
        data_sets.test = DataSet(test_images, test_labels)
        
    if (path_dir2):
        VALIDATION_IMAGES , VALIDATION_LABELS = Dataset_dir_validation(path_dir2)
        validation_image = rasterImage(VALIDATION_IMAGES, bands)    
        validation_labels = to_hot_vector(VALIDATION_LABELS,clases=n_clases)
        perm = np.arange(len(VALIDATION_IMAGES))  
        np.random.shuffle(perm)
        validation_image = validation_image[perm] 
        validation_labels=  validation_labels[perm]  
        data_sets.validation = DataSet(validation_image, validation_labels)          

    return data_sets
