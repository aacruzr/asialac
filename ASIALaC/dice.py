# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 09:52:14 2017

@author: gitecx
"""

from osgeo import gdal, ogr

import numpy as np



def map(extent,p):
    
    x_min = extent(0)
    x_max = extent(1)
    y_min = extent(2)
    y_max = extent(3)
    
    x_res = int((x_max - x_min) / 30)
    y_res = int((y_max - y_min) / 30)
    
    resolution=x_res*y_res

    map_out = np.zeros(resolution).reshape(x_res,y_res)
    
    
    for i in range (len(p)):
        xOffset = abs(int((float(p[i][0][0]) - x_min) / 30))
        yOffset = abs(int((float(p[i][1][0]) - y_max) / 30))
    
        for l in range (20):
            for k in range (20):
                
                if(yOffset<x_res -20 and xOffset<y_res-20):
                    map_out[yOffset+k][xOffset+l]=(p[i][2]+1)    

    np.save('/home/gitecx/Documents/Maps',map_out)
    
    
    
class Rasterize:
    
    def __ini__(self,path):
        source_ds = ogr.Open(path)
        self.source_layer = source_ds.GetLayer()
        self.x_min, self.x_max, self.y_min, self.y_max = self.source_layer.GetExtent()
        self.array=self.vetor_to_raster(path)        
        source_ds = None
        
    def vetor_to_raster(self,path):
        pixel_size = 30
        NoData_value = 0

        
        # Create the destination data source
        x_res = int((self.x_max - self.x_min) / pixel_size)
        y_res = int((self.y_max - self.y_min) / pixel_size)
        target_ds = gdal.GetDriverByName('MEM').Create('', x_res, y_res, gdal.GDT_Byte)
        target_ds.SetGeoTransform((self.x_min, pixel_size, 0, self.y_max, 0, -pixel_size))
        band = target_ds.GetRasterBand(1)
        band.SetNoDataValue(NoData_value)
        
        ## Rasterize
        gdal.RasterizeLayer(target_ds, [1], self.source_layer, options=["ATTRIBUTE=DN"])
     
        # leer como array
        array = band.ReadAsArray()

        target_ds = None
        
        return array


def coefficient2(path,path2):  
    dice = []  
    for l in range (1,5):
        
        A = Rasterize()
        A.__ini__(path)
        B = Rasterize()
        B.__ini__(path2)
        a = A.array
        b = B.array

        for i in range(1,5):
                       
            if (l != i):
                a[a==i]=0
                b[b==i]=0
                
        a[a==l]=1
        b[b==l]=1                
  
#        dice = np.sum(seg[gt==k])*2.0 / (np.sum(seg) + np.sum(gt))
        c=np.sum(b[a==1])*2.0 / (np.sum(a) + np.sum(b))
        
        dice.append(c)
            
    return dice


def coefficient(pred,path2):

    dice = []
    mask = []
    for l in range(1,5):
        A = Rasterize()
        A.__ini__(path2)                  
        a = A.array        
        b = np.copy(pred)
        
        for i in range(1,5):
                           
            if (l != i):

                a[a==i]=0
                b[b==i]=0
    
        a[a==l]=1
        b[b==l]=1                

#        dice = np.sum(seg[gt==k])*2.0 / (np.sum(seg) + np.sum(gt))
                
        c=np.sum(b[a==1])*2.0 / (np.sum(a) + np.sum(b))
        
        
        dice.append(c)
        mask.append(a)

    return dice,mask
                

#path2 = "/media/gitecx/Nuevo vol/Untitled Folder/000Nueva carpeta2/iMAGNEES DE SALIDA/s_out/tupa.shp"
#
#x = coefficient2(path,path2)
#
#
##imgplot = plt.imshow(a)
#
#
#
#
#import numpy as np
#
#import matplotlib.pyplot as plt
#import matplotlib.image as mpimg
#a=np.load('/home/gitecx/Documents/Maps/tuparro.npy')
#
#imgplot = plt.imshow(a)