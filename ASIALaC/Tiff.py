# -*- coding: utf-8 -*-
"""
Created on Tue Mar 22 16:26:38 2016

@author: arnol
"""
def createTiff(folders, parche, x0, y0, pixel_size, step, difBan,rotate):
    import osr
    import os 
    from osgeo import gdal
    array=parche
    name= "x="+str(x0)+",y="+str(y0)+"Size="+str(step)+"_"+str(rotate)+difBan+".tif"
    #format = "GTiff"
    direccion =os.path.join(folders,name)
    driver = gdal.GetDriverByName( "GTiff" )
    #metadata = driver.GetMetadata()
    dst_ds = driver.Create( direccion, step, step, len(parche) , gdal.GDT_Float32 )
    spatialReference =osr.SpatialReference() 
    spatialReference.ImportFromEPSG(3118)
    dst_ds.SetGeoTransform( [ x0, pixel_size, 0, y0, 0, -pixel_size] )
    srs = osr.SpatialReference()
    dst_ds.SetProjection( srs.ExportToWkt() )
    
    for i in range(len(array)):
        j=i+1
        dst_ds.GetRasterBand(j).WriteArray( array[i] )
    # Once we're done, close properly the dataset
    dst_ds = None
    
class Directory:
    
    def __init__(self):
        self.x0=0
        self.y0=0
        self.xi=0
        self.yi=0
        self.width=0
        self.high=0
        self.pixelwidth=0
        self.pixelhigh=0        
        self.band=0
        self.address=""

    def Bands( self,sourceimage):
        
        import  os
       
        dirs = os.listdir( sourceimage)  
        
        bands=[]        
        for filename in dirs:
            if ( (filename.find("aux.xml") < 0) & ( (filename[-4:]==".tif") | (filename[-4:]==".TIF")  )):
                bands.append(sourceimage+"/"+filename)                
        bands.sort()    
      # self.Features(bands[0])
        return bands
        
        
    def Features(self, dlist):
        
        self.address=dlist
        from osgeo import gdal
        dataset=gdal.Open(dlist)
        rband=dataset.GetRasterBand(1)
        geo=dataset.GetGeoTransform()       
        self.x0=geo[0]
        self.y0=geo[3]
        self.pixelwidth=geo[1]
        self.pixelhigh=abs(geo[5])
        self.width = dataset.RasterXSize
        self.high = dataset.RasterYSize    
        self.yi=self.y0-(self.pixelhigh*self.high)
        self.xi=self.x0+(self.pixelwidth*self.width)
         

    def Search(self):
#        sourceimage=[]
#        c=[]
#        import os
#        from Tkinter import Tk
#        import Tkconstants, tkFileDialog
#        from tkFileDialog import askopenfilename     
#        t=True
#        root = Tk() 
#        root.withdraw()
#        a = tkFileDialog.askdirectory(parent=root,initialdir=".",title='Please select a directory',mustexist=True)        
#        for x in os.walk(a):
#            if(x!=a):
#                sourceimage.append(x[0])     
#        for folders in range (1,len(sourceimage)):
#            c.append(self.Bands(sourceimage[folders]) )
#        print c
#        return c
        return [['/media/gitecx/Gits/Investigacion/Shape/Shape Altillanura/Tuparro/raster/RasterTuparro3118/LT50040562008021CUB00/LT50040562008021CUB00_B1.tif', '/media/gitecx/Gits/Investigacion/Shape/Shape Altillanura/Tuparro/raster/RasterTuparro3118/LT50040562008021CUB00/LT50040562008021CUB00_B2.tif', '/media/gitecx/Gits/Investigacion/Shape/Shape Altillanura/Tuparro/raster/RasterTuparro3118/LT50040562008021CUB00/LT50040562008021CUB00_B3.tif', '/media/gitecx/Gits/Investigacion/Shape/Shape Altillanura/Tuparro/raster/RasterTuparro3118/LT50040562008021CUB00/LT50040562008021CUB00_B4.tif', '/media/gitecx/Gits/Investigacion/Shape/Shape Altillanura/Tuparro/raster/RasterTuparro3118/LT50040562008021CUB00/LT50040562008021CUB00_B5.tif', '/media/gitecx/Gits/Investigacion/Shape/Shape Altillanura/Tuparro/raster/RasterTuparro3118/LT50040562008021CUB00/LT50040562008021CUB00_B6.tif', '/media/gitecx/Gits/Investigacion/Shape/Shape Altillanura/Tuparro/raster/RasterTuparro3118/LT50040562008021CUB00/LT50040562008021CUB00_B7.tif']]

        
    def searchFiles (self):
#        shapeFiles=[]
#        import os
#        from Tkinter import Tk
#        import Tkconstants, tkFileDialog
#        from tkFileDialog import askopenfilename     
#
#        root = Tk() 
#        root.withdraw()
#        a = tkFileDialog.askdirectory(parent=root,initialdir=".",title='Please select a directory',mustexist=True)        
#        for root, dirs, files in os.walk(a):
#            for name in files:
#                if(name.find(".shp") > 0):
#                    shapeFiles.append(os.path.join(root, name))
##        shapeFiles=[u'I:/Investigacion/Shape/Shape Altillanura/aa\\Afloramientos rocosos\\Afloramientos rocosos.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Bosque de galer\xeda y ripario\\Bosque de galer\xeda y ripario.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Bosque denso alto de tierra firme\\Bosque denso alto de tierra firme.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Bosque denso alto inundable\\Bosque denso alto inundable.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Herbazal Abierto Arenoso\\Herbazal Abierto Arenoso.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Herbazal denso de tierra firme no arbolado\\Herbazal denso de tierra firme no arbolado.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Herbazal denso inundable arbolado\\Herbazal denso inundable arbolado.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Herbazal denso inundable no arbolado\\Herbazal denso inundable no arbolado.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Lagunas, lagos y ci\xe9nagas naturales\\Lagunas, lagos y ci\xe9nagas naturales.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Mosaico de pastos con espacios naturales\\Mosaico de pastos con espacios naturales.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\R\xedos\\R\xedos.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Vegetaci\xf3n secundaria  o en transici\xf3n\\Vegetaci\xf3n secundaria  o en transici\xf3n.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Zonas arenosas naturales\\Zonas arenosas naturales.shp', u'I:/Investigacion/Shape/Shape Altillanura/aa\\Zonas quemadas\\Zonas quemadas.shp']           
#        print shapeFiles
#        return shapeFiles
        return ["/media/gitecx/Nuevo vol/Untitled Folder/000Nueva carpeta2/Final Shape's/Clases_entrenamiento/Tupa2_NIVEL_2__Aguas Continentales.shp"]
#                "/media/gitecx/Nuevo vol/Untitled Folder/000Nueva carpeta2/Final Shape's/Clases_entrenamiento/Tupa2_NIVEL_2__reas Abiertas sin o con poca Vegetacin.shp"]
#        return ["/media/gitecx/Nuevo vol/Untitled Folder/000Nueva carpeta2/Final Shape's/Clases_entrenamiento/Tupa2_NIVEL_2__Bosques.shp"]
#        return ["/media/gitecx/Nuevo vol/Untitled Folder/000Nueva carpeta2/Final Shape's/Clases_entrenamiento/Tupa2_NIVEL_2__reas con Vegetacin Herbacea y Arbustiva.shp"]


class Mcobertura:
    
    def __init__(self, sourceShape):
        from osgeo import ogr
        import os
#        h=Directory()
        
     #   os.chdir(r"I:\Investigacion\Shape\CAHUINARÍ")
        self.daShapefile = sourceShape
        # Abrir el Shapefile y tipo de archivo
        self.shape=ogr.GetDriverByName('ESRI Shapefile')
        self.dataSource = self.shape.Open(self.daShapefile, 0)
        # Guarda el numero de layers ("Caracteristicas") del poligono
        self.layer = self.dataSource.GetLayer()
        self.numPolygon = self.layer.GetFeatureCount()
        # Guarda los nombres de las columnass
        self.layerDefinition = self.layer.GetLayerDefn()
        
        self.outShapefile ="" 
        self.outDriver = ogr.GetDriverByName("ESRI Shapefile")

    def contar(self,elemento, lista):
        veces = 0
        for i in lista:
            if elemento == i:
                veces += 1
        return veces

    def nameColum(self):  
        columnas=[]        
        for i in range(self.layerDefinition.GetFieldCount()):
            columnas.append(self.layerDefinition.GetFieldDefn(i).GetName())
        return columnas
      
    def numeroClases(self):
        # Direccion de los archivos          
        s = "LEYENDA"
        ss=[]
        rlist=set()
        for feature in self.layer:
            ss.append(feature.GetField(s))
           
        rlist = [x for x in ss if x not in rlist and not rlist.add(x)]    
        nClases=[]
        for i in rlist:
            nClases.append([i,self.contar(i,ss)]) 
        return nClases                 
        
    def polyShape(self, a, b):
        from osgeo import ogr
        from osgeo import osr
        import os
        self.outShapefile =os.path.join( os.path.split( self.daShapefile )[0], b )
        self.layer.SetAttributeFilter("minor = 'HYDR'")
        if os.path.exists(self.outShapefile):
            self.outDriver.DeleteDataSource(self.outShapefile)
        

        srs = osr.SpatialReference()
        srs.ImportFromEPSG(3118)
        
        outDataSource = self.outDriver.CreateDataSource(self.outShapefile)
        outShape = os.path.splitext( os.path.split( self.outShapefile )[1] )[0]

        outLayer = outDataSource.CreateLayer( outShape, srs, geom_type=ogr.wkbMultiPolygon )
        
        outLayerDefn = outLayer.GetLayerDefn()
        for inFeature in self.layer:     
            # Create output Feature
            if inFeature.GetField("LEYENDA")==b:          
                outFeature = ogr.Feature(outLayerDefn)
                # Set geometry as centroid
                geom = inFeature.GetGeometryRef()
                outFeature.SetGeometry(geom.Clone())
                # Add new feature to output Layer
                outLayer.CreateFeature(outFeature)
                # Close DataSources
        #self.shape.Destroy()
        outDataSource.Destroy()
        
        
#############################################################################################
                
class Patch (Directory):
    
    def __init__(self, folder):
        from osgeo import ogr
        self.daShapefile = folder
        # Abrir el Shapefile y tipo de archivo, leer el extent
        self.shape=ogr.GetDriverByName('ESRI Shapefile')
        self.dataSource = self.shape.Open(self.daShapefile)
        self.layer = self.dataSource.GetLayer()
        self.xmin, self.xmax, self.ymin, self.ymax = self.layer.GetExtent()
        

    def extractMatrix (self, pixel_size, pace, overlap, overSampling, shape):
        from osgeo import gdal
        from  scipy import ndimage
        from scipy.ndimage import label
        overLap=0
        avr=80
        if (shape=="/media/gitecx/Nuevo vol/Untitled Folder/000Nueva carpeta2/Final Shape's/Clases_entrenamiento/Tupa2_NIVEL_2__Aguas Continentales.shp"):
            avr=65
            print ("si")

        if(overlap > 0):
            if(overlap <= 90):
                overLap=(overlap*pace)/100
        # tamaño de los pixeles y valores sin dato
        pixel_size = 30
        NoData_value = 255 
        # crear data source de salida
        x_res = int((self.xmax - self.xmin) / pixel_size)
        y_res = int((self.ymax - self.ymin) / pixel_size)

        dataSource = gdal.GetDriverByName('MEM').Create('', x_res, y_res, gdal.GDT_Byte)
        dataSource.SetGeoTransform((self.xmin, pixel_size, 0, self.ymax, 0, -pixel_size))
        band = dataSource.GetRasterBand(1)
        band.SetNoDataValue(NoData_value)
        # rasterizar
        gdal.RasterizeLayer(dataSource, [1], self.layer, burn_values=[1])        
        # leer como array
        geo = dataSource.GetGeoTransform()  
        array = band.ReadAsArray()
        arraylist=[]  
        step=int(pace)    
        labeled_array, num_features = label(array)
        count=ndimage.find_objects(labeled_array)
        
        for tuples in count:   
            for l in xrange(tuples[0].start,tuples[0].stop,int(step-overLap)):
                for s in xrange(tuples[1].start,tuples[1].stop,step-overLap):
                    if(overSampling==False):                    
                        A=array[l:l+step, s:s+step]  
                        if (self.sumM(A) >=avr ):  
                            xmin=(s*pixel_size)+geo[0]
                            ymax=geo[3]-(pixel_size*l)
                            arraylist.append([xmin,ymax])
                    
                    if(overSampling==True):                    
                          
                        for i in range(9):
                            if i == 0:
                                A=array[l:l+step, s:s+step] 
                                if (self.sumM(A) >=avr ):                              
                                    xmin=(s*pixel_size)+geo[0]
                                    ymax=geo[3]-(pixel_size*l)
                                    arraylist.append([xmin,ymax])

                            if i == 1:
                                A=array[l-2:l+step-2, s:s+step] 
                                if (self.sumM(A) >=avr ):                              
                                    xmin=(s*pixel_size)+geo[0]
                                    ymax=geo[3]-(pixel_size*(l-2))
                                    arraylist.append([xmin,ymax])

                            if i == 2:
                                A=array[l+2:l+step+2, s:s+step] 
                                if (self.sumM(A) >=avr ):                              
                                    xmin=(s*pixel_size)+geo[0]
                                    ymax=geo[3]-(pixel_size*(l+2))
                                    arraylist.append([xmin,ymax])
                                
                            if i == 3:
                                A=array[l+2:l+step+2, s-2:s+step-2] 
                                if (self.sumM(A) >=avr ):                              
                                    xmin=((s-2)*pixel_size)+geo[0]
                                    ymax=geo[3]-(pixel_size*(l+2))
                                    arraylist.append([xmin,ymax])
 
                            if i == 4:
                                A=array[l+2:l+step+2, s+2:s+step+2] 
                                if (self.sumM(A) >=avr ):                              
                                    xmin=((s+2)*pixel_size)+geo[0]
                                    ymax=geo[3]-(pixel_size*(l+2))
                                    arraylist.append([xmin,ymax])

                            if i == 5:
                                A=array[l-2:l+step-2, s-2:s+step-2] 
                                if (self.sumM(A) >=avr ):                              
                                    xmin=((s-2)*pixel_size)+geo[0]
                                    ymax=geo[3]-(pixel_size*(l-2))
                                    arraylist.append([xmin,ymax])
                               
                            if i == 6:
                                A=array[l:l+step, s-2:s+step-2] 
                                if (self.sumM(A) >=avr ):                              
                                    xmin=((s-2)*pixel_size)+geo[0]
                                    ymax=geo[3]-(pixel_size*l)
                                    arraylist.append([xmin,ymax])

                            if i == 7:
                                A=array[l:l+step, s+2:s+step+2] 
                                if (self.sumM(A) >=avr ):                              
                                    xmin=((s+2)*pixel_size)+geo[0]
                                    ymax=geo[3]-(pixel_size*(l))
                                    arraylist.append([xmin,ymax])
 
                            if i == 8:
                                A=array[l-2:l+step-2, s+2:s+step+2] 
                                if (self.sumM(A) >=avr ):                              
                                    xmin=((s+2)*pixel_size)+geo[0]
                                    ymax=geo[3]-(pixel_size*(l-2))
                                    arraylist.append([xmin,ymax])
         
        return arraylist

     
    def reductionPach (self, plist, value):
        import random
        import numpy as np
        
        b=len(plist)-value
        if(b > 0): 
            
            limitR = range(len(plist))  
            random_index = random.sample(limitR, b)
            arraylistN=np.delete(plist,random_index,0)
            return arraylistN
        else:
            return plist
    
    
    def sumM (self, matrix):
        import numpy
        x=(matrix.mean())*100
        return x
        
##################################################################
### GETATTRIBUTE !!!!
####################################################################        
class GetAttribute(Directory, Patch):
    def __init__(self):
        self.atributos=[]
        self.shapeFiles=[]

    def listRaster(self):
        print "Select the folder with the raster "
        h=self.Search()
#        h=[[u'I:/RasterCobert2012/pSNSM\\852/B1.tif', u'I:/RasterCobert2012/pSNSM\\852/B2.tif', u'I:/RasterCobert2012/pSNSM\\852/B3.tif', u'I:/RasterCobert2012/pSNSM\\852/B4.tif', u'I:/RasterCobert2012/pSNSM\\852/B5.tif', u'I:/RasterCobert2012/pSNSM\\852/B6.tif', u'I:/RasterCobert2012/pSNSM\\852/B7.tif'], [u'I:/RasterCobert2012/pSNSM\\853/B1.tif', u'I:/RasterCobert2012/pSNSM\\853/B2.tif', u'I:/RasterCobert2012/pSNSM\\853/B3.tif', u'I:/RasterCobert2012/pSNSM\\853/B4.tif', u'I:/RasterCobert2012/pSNSM\\853/B5.tif', u'I:/RasterCobert2012/pSNSM\\853/B6.tif', u'I:/RasterCobert2012/pSNSM\\853/B7.tif']]
        self.atributos=[[]for _ in range(len(h)) ]

        for x in range (0,len(h)):
            for l in range (0, len(h[x])):
                j=Directory() 
                j.Features(h[x][l])
                self.atributos[x].append(j)
        print "Select the shape files to use "
#        self.searchFiles=[u'I:/Coberturas/conjunto de validacion/cTEMP\\Bosque Denso Alto Inundable\\Bosque Denso Alto Inundable.shp', u'I:/Coberturas/conjunto de validacion/cTEMP\\Herbazal Denso Inundable Arbolado\\Herbazal Denso Inundable Arbolado.shp', u'I:/Coberturas/conjunto de validacion/cTEMP\\Herbazal Denso Inundable no Arbolado\\Herbazal Denso Inundable no Arbolado.shp', u'I:/Coberturas/conjunto de validacion/cTEMP\\Rios\\Rios.shp']
        self.shapeFiles=self.searchFiles()
        return  self.atributos
    
    def createFolder (self, shapeFiles, class_number ):
        import os      
        f=os.path.split(shapeFiles)
        class_name=f[1][:-4]
        number=class_number+1
        directoryFolder="/media/gitecx/Nuevo vol/ASIAlac/"+"Patch/"+"Class_"+"0"+str(number)+"_"+class_name
        if not os.path.exists(directoryFolder):
            os.makedirs(directoryFolder)
        return directoryFolder
        
    def runTime (self, step, overlap, pixel_size, overSampling,flip):
        folderClass=""
        cuenta=0
        for shapeFile in self.shapeFiles:
            count=self.shapeFiles.index(shapeFile)
            Patch.__init__(self,shapeFile)           
            folderClass=self.createFolder(shapeFile,count)    
            position=self.extractMatrix(pixel_size,step,overlap, overSampling, shapeFile) 
            print shapeFile
            print(len(position))
#            if (position!=None):
#                for i in range (len(position)):
#                    self.extractRaster(position[i][0],position[i][1],pixel_size,step, folderClass, overSampling, len(position),flip) 

    def extractRaster(self, x, y, pixel_size, step,folderClass, overSampling, numberP,flip):

        from osgeo import gdal
        rotate=0
       
        for i in range(len(self.atributos)):
            arrayP=[]  
            difBan=""
            for l in range(len(self.atributos[i])):
                xi=x+(step*self.atributos[i][l].pixelwidth)
                yi=y-(step*self.atributos[i][l].pixelwidth)
                dataset=gdal.Open(self.atributos[i][l].address)
                rband=dataset.GetRasterBand(1)                 
                if( (x > self.atributos[i][l].x0) & (xi < self.atributos[i][l].xi) & (y < self.atributos[i][l].y0) & (yi > self.atributos[i][l].yi)):
 
                    if((self.atributos[i][l].pixelwidth < 31) ):
                   
                        xOffset = int((x - self.atributos[i][l].x0) /self.atributos[i][l].pixelwidth)
                        yOffset = int((y - self.atributos[i][l].y0) / (-self.atributos[i][l].pixelwidth))
                        array=rband.ReadAsArray(xOffset,yOffset,step,step)                           

                        t= self.maskBinary(array)
                        if (t==True):                
                            arrayP.append(array)
                            difBan=str(i)+","+str(l)                
                    dataset= None 
            if (overSampling==True):
                if(flip==True):
                    if(len(arrayP) ==7):
                        
                        rotation, flip= self.rotatePach(arrayP, numberP)
                        
                        createTiff(folderClass,rotation[0:7],x,y,pixel_size,step, difBan,rotate)
                        createTiff(folderClass,rotation[7:14],x,y,pixel_size,step, difBan,90)
                        createTiff(folderClass,rotation[14:21],x,y,pixel_size,step, difBan,180)
                        createTiff(folderClass,rotation[21:28],x,y,pixel_size,step, difBan,270)
                        
                        createTiff(folderClass,flip[0:7],x,y,pixel_size,step, difBan,-1)
                        createTiff(folderClass,flip[7:14],x,y,pixel_size,step, difBan,-90)
                        createTiff(folderClass,flip[14:21],x,y,pixel_size,step, difBan,-180)
                        createTiff(folderClass,flip[21:28],x,y,pixel_size,step, difBan,-270)
                else:
                    if(len(arrayP) ==7):
                        
                        createTiff(folderClass,arrayP,x,y,pixel_size,step, difBan,rotate)

            else:
                if(len(arrayP) ==7):
                    createTiff(folderClass,arrayP,x,y,pixel_size,step, difBan,rotate)

    def rotatePach(self, arrayP, numberP):
        import numpy as np
        array=arrayP
        array270=[]
        array180=[]
        array90=[]
        array2=[]   
        
        for i in range(len(array)):
            array270.append(np.rot90(array[i]))

        for i in range(len(array)):
            array180.append(np.rot90(array[i],2))
       
        for i in range(len(array)):
            array90.append(np.rot90(array[i],3))
            
        array+=array90+array180+array270
    
        for i in range(len(array)):
            array2.append(np.flipud(array[i]))    
        
        return array, array2
#        array180=[]
#        array90=[]
        
    
    def maskBinary (self, array):
        from scipy import ndimage
        valueMin=ndimage.minimum(array)
        valueMax=ndimage.maximum(array)
        if ((valueMin ==0) | (valueMax ==255) | (valueMax ==65535) ):
            return False
        else: return True
        
    def increaseArrayR(self, array, scalar,step): 
        import numpy as np
        arrayI=np.repeat(array,scalar)
        arrayOut=arrayI.reshape(step,step)
        return arrayOut



###################################################

# def runTime (self, step./modelos_clases_4/model_B_7_c1_128_c2_256_fc_1024_learning_rate_1e-06_dropout_0.75_decay_0.9_epsilon_1e-08_bath_100_c_.ckpt, overlap, pixel_size, overSampling):
a=GetAttribute()
c=a.listRaster()
a.runTime(20,75,30.0,True,True)

