# -*- coding: utf-8 -*-
"""
Created on Fri Feb 24 10:51:50 2017

@author: gitecx
"""

from osgeo import gdal
import sklearn as sk
# import input_test
import io
import numpy as np
import tensorflow as tf
import time
from sklearn.metrics import confusion_matrix

c1=128
c2=256
fc=1024
b1=128
b2=256
b3=1024
n_classes = 4
bands = 7
model_path='/media/gitecx/Nuevo vol/ASIAlac/Modelos/modelos_20x20_4/model_B_7_c1_128_c2_256_fc_1024_learning_rate_1e-05_dropout_0.5_decay_0.96_epsilon_1e-08_bath_392_ep_66.ckpt'

def conv2d(img, w, b):
    return tf.nn.relu(tf.nn.bias_add(tf.nn.conv2d(img, w, strides=[1, 1, 1, 1], padding='VALID'),b))

def max_pool(img):
    return tf.nn.max_pool(img, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='VALID')

def conv_net2(images,w,b,_dropout):
    # Conv 1
    conv1 = conv2d(images,w['wc1'], b['bc1'])    
    # Max Pooling (down-sampling)
    conv1 = max_pool(conv1)
    # Apply Dropout
    conv1 = tf.nn.dropout(conv1, _dropout)
    
    #Conv 2
    conv2 = conv2d(conv1, w['wc2'], b['bc2'])
    # Max Pooling (down-sampling)
    conv2 = max_pool(conv2)
    
    # Apply Dropout
    conv2 = tf.nn.dropout(conv2, _dropout)

    # Fully connected layer
    # Reshape conv2 output to fit dense layer input
    dense1 = tf.reshape(conv2, [-1, w['wd1'].get_shape().as_list()[0]]) 
        
    # Relu activation
    dense1 = tf.nn.relu(tf.add(tf.matmul(dense1, w['wd1']),b['bd1']))
    # Apply Dropout
    dense1 = tf.nn.dropout(dense1, _dropout) # Apply Dropout

    # Output, class prediction
    out = tf.add(tf.matmul(dense1, w['out']),b['out'])
    return out



keep_prob = tf.placeholder(tf.float32) 
# tf Graph input
x = tf.placeholder(tf.float32,[None,20,20,bands])
y = tf.placeholder("float", [None, n_classes])

patch = tf.placeholder("float", [1,20, 20, 7])
    
weights = {  
   # 5x5 conv, 7 input, 128 outputs
    'wc1': tf.Variable(tf.random_normal([5, 5, bands, c1])), 
     # 5x5 conv, 128 inputs, 256 outputs
    'wc2': tf.Variable(tf.random_normal([5, 5, c1, c2])), 
    # fully connected, 7*7*64 inputs, 1024 outputs
    'wd1': tf.Variable(tf.random_normal([2*2*c2, fc])), 
        # 1024 inputs, 10 outputs (class prediction)
    'out': tf.Variable(tf.random_normal([fc, n_classes])) 
}

biases = {
    'bc1': tf.Variable(tf.random_normal([b1])),
    'bc2': tf.Variable(tf.random_normal([b2])),
    'bd1': tf.Variable(tf.random_normal([b3])),
    'out': tf.Variable(tf.random_normal([n_classes]))
    }
saver = tf.train.Saver()

pred = conv_net2(x,weights,biases, keep_prob) 

# Initializing the variables
init = tf.global_variables_initializer()
saver = tf.train.Saver()


def raster():
    

    r= "/media/gitecx/Gits/Investigacion/Tuparro7/tuparro.vrt"
    ds = gdal.Open(r)
    
    geo=ds.GetGeoTransform()       
    x0=geo[0]
    y0=geo[3]
    pixelwidth=geo[1]
    pixelhigh=abs(geo[5])
    width = ds.RasterXSize
    high = ds.RasterYSize    
    yi=y0-(pixelhigh*high)
    xi=x0+(pixelwidth*width)          
    return [x0,y0,xi,yi]

def maskBinary (array):

    from scipy import ndimage
    valueMin=ndimage.minimum(array)
    valueMax=ndimage.maximum(array)
    if ((valueMin ==0) | (valueMax ==255) | (valueMax ==65535) ):
        return False
    else: return True
    

class Path:
    
    def __init__(self, folder):
        from osgeo import ogr
        self.daShapefile = folder
        # Abrir el Shapefile y tipo de archivo, leer el extent
        self.shape=ogr.GetDriverByName('ESRI Shapefile')
        self.dataSource = self.shape.Open(self.daShapefile)
        self.layer = self.dataSource.GetLayer()
        self.xmin, self.xmax, self.ymin, self.ymax = self.layer.GetExtent()
        

    def extractMatrix (self, pixel_size, pace, overlap, overSampling,):
        
        from  scipy import ndimage
        from scipy.ndimage import label
        overLap=0
        avr=80

        if(overlap > 0):
            if(overlap <= 90):
                overLap=(overlap*pace)/100
        # tamaño de los pixeles y valores sin dato

        pixel_size = 30
        NoData_value = 255 
        
        # crear data source de salida
        x_res = int((self.xmax - self.xmin) / pixel_size)
        y_res = int((self.ymax - self.ymin) / pixel_size)

        dataSource = gdal.GetDriverByName('MEM').Create('', x_res, y_res, gdal.GDT_Byte)
        dataSource.SetGeoTransform((self.xmin, pixel_size, 0, self.ymax, 0, -pixel_size))
        band = dataSource.GetRasterBand(1)
        band.SetNoDataValue(NoData_value)
        # rasterizar
        gdal.RasterizeLayer(dataSource, [1], self.layer, burn_values=[1])        
        # leer como array
        geo = dataSource.GetGeoTransform()  
        array = band.ReadAsArray()
        arraylist=[]  
        step=int(pace)    
        labeled_array, num_features = label(array)
        count=ndimage.find_objects(labeled_array)
       
        print array.shape
        for tuples in count:   
            for l in xrange(tuples[0].start,tuples[0].stop,int(step-overLap)):
                for s in xrange(tuples[1].start,tuples[1].stop,step-overLap):
                    if(overSampling==False):                    
                        A=array[l:l+step, s:s+step]  
                        xmin=(s*pixel_size)+geo[0]
                        ymax=geo[3]-(pixel_size*l)
                        arraylist.append([xmin,ymax])
                        
        return arraylist
 
    
    
def raster2(position,i):
    
    a = position[i][0]
    b = position[i][1]  
                
    xOffset = int((a - rlimit[0]) / 30)
    yOffset = int((b - rlimit[1]) / (-30))
        
    r= "/media/gitecx/Gits/Investigacion/Tuparro7/tuparro.vrt"
    ds = gdal.Open(r)
    bands=[]
    for i in range(ds.RasterCount):   
        band = ds.GetRasterBand(i+1)
        bands.append((band.ReadAsArray(xOffset,yOffset,20,20))) 

    myarray = np.asarray(bands)  
    
    for i in range(1,8):
        m =np.mean(myarray[:,i])
        s =np.std(myarray[:,i])
        myarray[:,i]=((myarray[:,i]-m)/s)
        
    image = np.expand_dims(myarray, axis=0)
    image = np.transpose(image, (0, 2,3,1 ))  
         
    return image,a,b
        

a = Path("/media/gitecx/Nuevo vol/Untitled Folder/000Nueva carpeta2/iMAGNEES DE SALIDA/s_out/Final Shape's/tupa.shp")
position = a.extractMatrix(30,20,0, overSampling=False) 

extent = [ 905944.49,
          1028644.09,
          1043454.11,
          1107237.21 ]
predictions = []


# map_out = np.zeros(resolution).reshape(y_res,x_res)
rlimit = raster()
    
print len(position)       

#start= 0
#
#for i in range (22):
#    stop = start + 1000
#    with tf.Session() as sess :
#        sess.run(init)
#        prediction  = tf.argmax(pred,1)
#        saver.restore(sess,model_path)   
#        print  ("Model successfully restored")
#        start_time = time.clock()                 
#        for i in range(start, stop):
#            
#            if i==start:
#                print str(start)+ " "+ str(stop)
#        
##            start_time = time.clock()   
#                          
#            image,a,b = raster2(position,i)
#
#            labels = sess.run([prediction],feed_dict={x: image, keep_prob: 1.})
#
#
#            predictions.append([labels[0],a,b])
#        
#        end_time = time.clock()
#        print "time this epoch=", (end_time-start_time)  
#        start += stop
#        print len(predictions)
#
#        sess.close          
#
#with io.FileIO("Predictions.txt", "w") as file:
#    for i in range(len(predictions)):
#        
#        pr = "X = "+ str(i[1])+ " Y = "+ str(i[2])+ " Etiqueta = " + str(i[0])        
#        file.write(pr + "\n")        
#        
#    file.close    
    #

a = Path("/media/gitecx/Nuevo vol/Untitled Folder/000Nueva carpeta2/iMAGNEES DE SALIDA/s_out/Final Shape's/tupa.shp")
position = a.extractMatrix(30,20,0, overSampling=False) 

extent = [ 905944.49,
          1028644.09,
          1043454.11,
          1107237.21 ]
predictions = []


# map_out = np.zeros(resolution).reshape(y_res,x_res)
rlimit = raster()
    
print len(position)       

with tf.Session() as sess :
    sess.run(init)
    prediction  = tf.argmax(pred,1)
    saver.restore(sess,model_path)   
    print  ("Model successfully restored")
    start_time = time.clock()                 
    for i in range(1000):
        
  
        image,a,b = raster2(position,i)
        
            
        if (image.shape == (1,20,20,7)):
        
            labels = sess.run([prediction],feed_dict={x: image, keep_prob: 1.})
        
            if (i%1000 == 0 ):
                print (labels[0])    
                end_time = time.clock()
                print "time this epoch=", (end_time-start_time)                
            predictions.append(labels[0])
            posiciones

    sess.close          

with io.FileIO("Predictions.txt", "w") as file:
    for i in range(len(predictions)):
        print i
    
        
#        pr = "X = "+ str(i[1])+ " Y = "+ str(i[2])+ " Etiqueta = " + str(i[0][0])        
#        file.write(pr + "\n")        
#        
    file.close    
    
    
hola=[]    
for i in range(3)    :
    for l in range (5,7):
        hola.append([i,l])
    
    
    

import numpy as np

with open(c, "r") as f:
    array = []
    for line in f:
        array.append(line)


posicion=[]
labels=[]
for i in array:
    x=i.find('X = ')
    y=i.find('Y = ')
    z=i.find('Etiqueta =')
    x0=i[x+4:y-1]
    y0=i[y+4:z]
    e=i[z+12:z+13]
    posicion.append([x0,y0])        
    labels.append(e)

    
l=np.asarray(labels)    
#min(l)

extent = [ 905944.49,
          1028644.09,
          1043454.11,
          1107237.21 ]

#xMin,yMin 905944.49,1043454.11 : xMax,yMax 1028644.09,1107237.21
xmax=extent[2]
xmin=extent[0]
ymax=extent[3]
ymin=extent[1]

xres=int((xmax-xmin)/30)
yres=int((ymax-ymin)/30)

shape=np.zeros(xres*yres).reshape(yres,xres)

for i in range(len(labels)): 
    a=float(posicion[i][0])
    b=float(posicion[i][1])
    xOffset = int((a-xmin)/30)
    yOffset = int((b-ymax)/(-30))
    if i==0:
        print yOffset
    shape[yOffset:(yOffset+20),xOffset:(xOffset+20)]=int(labels[i])    
    
    
%matplotlib 

import matplotlib
import matplotlib.pyplot as plt

imgplot=plt.imshow(shape)